#!/usr/bin/env python3
# Python program for Detection of a
# specific color(blue here) using OpenCV with Python
import sys
import cv2
import numpy as np
 
# Webcamera no 0 is used to capture the frames
cap = cv2.VideoCapture(0)
xres = 640
yres = 480
fps = 5
cap.set(3, xres)
cap.set(4, yres)
cap.set(5, fps)

ylim = int(yres*0.8)
a = 0
buflen = 10
buf = [0] * buflen
clrgut = (255, 255, 255)
clrbad = ( 0, 0, 255)
clr = clrgut

while(cap.isOpened()):
    # Captures the live stream frame-by-frame
    ret, frame = cap.read()
    blrd = cv2.blur(frame,(10,10))

    # Converts images from BGR to HSV
    hsv = cv2.cvtColor(blrd, cv2.COLOR_BGR2HSV)

    #define yellow
    #lower_yellow = np.array([15,150,50])
    #upper_yellow = np.array([40,255,255])
    lower_yellow = np.array([160,120,50])
    upper_yellow = np.array([175,255,255])

    #define mask and detect contours based on the mask
    mask = cv2.inRange(hsv, lower_yellow, upper_yellow)
    contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL,
                                           cv2.CHAIN_APPROX_NONE)
    output = frame.copy()
    if len(contours) != 0:
        # the contours are drawn here
        c = max(contours, key = cv2.contourArea)
        cm = cv2.moments(c)
        cv2.drawContours(output, [c], -1, (200,200,0), 4)
        if cm['m00'] != 0 and cv2.contourArea(c) > 1000:
            cmy = int(cm['m01']/cm['m00'])
            a = (a+1) % buflen
            buf[a] = cmy
            if all(i < ylim for i in buf):
                clr = clrgut
            elif all(i > ylim for i in buf):
                clr = clrbad
            cv2.circle(output, (int(cm['m10']/cm['m00']), cmy), 8, clr, -1)
            cv2.line(output, (0, ylim), (xres, ylim), (0, 150, 0), 3)
    cv2.imshow('conts',output)
 
    #res = cv2.bitwise_and(frame,frame, mask= mask)
    #cv2.imshow('blurred',blrd)
    #cv2.imshow('frame',frame)
    #cv2.imshow('mask',mask)
    #cv2.imshow('res',res)
 
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break
 
# Destroys all of the HighGUI windows.
cv2.destroyAllWindows()
 
# release the captured frame
cap.release()
